﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReactUploads.Data
{
    public class Attachment
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Path { get; set; }
        public string File { get; set; }
        public string Name { get; set; }
        public DateTime DateUploaded { get; set; }
    }
}
