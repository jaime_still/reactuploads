﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ReactUploads.Data;
using ReactUploads.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReactUploads.Web.Extensions
{
    public static class AttachmentExtensions
    {
        private static string uploadBase = @"D:\Apps\ReactUploads\";
        private static string drivePathPattern = @"^[a-zA-Z]:";
        private static string urlFriendlyPattern = "[^a-zA-Z0-9-.]";

        public static async Task<List<Attachment>> GetAttachments(this AppDbContext db)
        {
            var attachments = await db.Attachments
                .OrderBy(x => x.File)
                .ToListAsync();

            return attachments;
        }

        public static async Task UploadAttachments(this AppDbContext db, IFormFileCollection files)
        {
            foreach (var file in files)
            {
                await db.AddAttachment(file);
            }
        }

        public static async Task RemoveAttachment(this AppDbContext db, Attachment model)
        {
            await model.DeleteFile();
            var attachment = await db.Attachments.FindAsync(model.Id);
            db.Attachments.Remove(attachment);
            await db.SaveChangesAsync();
        }

        private static async Task AddAttachment(this AppDbContext db, IFormFile file)
        {
            var upload = await file.WriteFile(uploadBase);

            var attachment = new Attachment
            {
                File = upload.Name,
                Name = upload.DisplayName,
                Url = upload.Url,
                Path = upload.Path,
                DateUploaded = DateTime.Now
            };

            await db.Attachments.AddAsync(attachment);
            await db.SaveChangesAsync();
        }

        private static async Task<UploadModel> WriteFile(this IFormFile file, string uploadUrl)
        {
            if (!(Directory.Exists(uploadUrl)))
            {
                Directory.CreateDirectory(uploadUrl);
            }

            var upload = await file.CreateUploadModel(uploadUrl);

            using (var stream = new FileStream(upload.Path, FileMode.Create))
            {
                await file.CopyToAsync(stream);
            }

            return upload;
        }

        private static Task<UploadModel> CreateUploadModel(this IFormFile file, string uploadUrl)
        {
            return Task.Run(() =>
            {
                var name = file.CreateSafeName(uploadUrl);

                var model = new UploadModel
                {
                    Name = name,
                    DisplayName = file.Name,
                    Path = $"{uploadUrl}{name}",
                    Url = ($"{uploadUrl}{name}").CheckUrlPath()
                };

                return model;
            });
        }

        private static string CheckUrlPath(this string url)
        {
            if (Regex.Match(url, drivePathPattern).Success)
            {
                var newPath = $@"file:///{url}";
                return Regex.Replace(newPath, @"\\", "/");
            }

            return url;
        }

        private static string CreateSafeName(this IFormFile file, string uploadUrl)
        {
            var increment = 0;
            var fileName = file.FileName.UrlEncode();
            var newName = file.FileName.UrlEncode();

            while (File.Exists(uploadUrl + newName))
            {
                increment++;
                var extension = fileName.Split('.').Last();
                newName = $"{fileName.Replace($".{extension}", "")}_{increment}.{extension}";
            }

            return newName;
        }

        private static Task DeleteFile(this Attachment attachment)
        {
            return Task.Run(() =>
            {
                try
                {
                    if (File.Exists(attachment.Path))
                    {
                        File.Delete(attachment.Path);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error Deleting File", ex);
                }
            });
        }

        private static string UrlEncode(this string url)
        {
            var friendlyUrl = Regex.Replace(url, @"\s", "-").ToLower();
            friendlyUrl = Regex.Replace(friendlyUrl, urlFriendlyPattern, string.Empty);
            return friendlyUrl;
        }
    }
}
