﻿using Microsoft.AspNetCore.Mvc;
using ReactUploads.Data;
using ReactUploads.Web.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactUploads.Web.Controllers
{
    [Route("api/[controller]")]
    public class AttachmentController : Controller
    {
        private AppDbContext db;

        public AttachmentController(AppDbContext db)
        {
            this.db = db;
        }

        [HttpGet("[action]")]
        public async Task<List<Attachment>> GetAttachments() => await db.GetAttachments();

        [HttpPost("[action]")]
        public async Task UploadAttachments()
        {
            var files = Request.Form.Files;

            if (files.Count < 1)
            {
                throw new Exception("No files provided to UploadAttachments API");
            }

            await db.UploadAttachments(files);
        }

        [HttpPost("[action]")]
        public async Task RemoveAttachment([FromBody]Attachment attachment)
        {
            await db.RemoveAttachment(attachment);
        }
    }
}
