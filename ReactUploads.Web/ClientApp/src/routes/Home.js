﻿import React from 'react';
import AttachmentService from '../services/AttachmentService';
import AddAttachments from '../components/AddAttachments';
import FileList from '../components/FileList';
import AttachmentList from '../components/AttachmentList';

const service = new AttachmentService();

export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            attachments: null,
            loading: true
        };
    }
    componentDidMount = async () => {
        await service.getAttachments();
        service.attachments.subscribe((data) => this.setState({ attachments: data }));
        this.setState({ loading: false });
    }

    clearFileState = () => {
        this.setState({
            fileList: null,
            formData: null
        });
    }

    onFileSelect = (e) => {
        this.setState({
            fileList: e[0],
            formData: e[1]
        });
    }

    onClearFiles = () => {
        this.clearFileState();
    }

    onUpload = async () => {
        this.setState({ loading: true });
        const res = await service.uploadAttachments(this.state.formData);
        this.setState({ loading: false });        
        if (res) this.clearFileState();
    }

    onRemoveAttachment = async (attachment) => {
        this.setState({ loading: true });
        await service.removeAttachment(attachment);
        this.setState({ loading: false });
    }

    renderUi = () => (
        <React.Fragment>
            <h2>Add Attachments</h2>
            <AddAttachments onFileSelect={this.onFileSelect}
                onClearFiles={this.onClearFiles}
                onUpload={this.onUpload}
                hasFiles={this.state.fileList && this.state.fileList.length > 0}
                loading={this.state.loading} />
            <FileList list={this.state.fileList} />
            <h2>Attachments</h2>
            <AttachmentList loading={this.state.loading} attachments={this.state.attachments} onRemoveAttachment={this.onRemoveAttachment} />
        </React.Fragment>
    );

    render = () => (
        <section className="wrapper">
            <h1>Home</h1>
            { this.renderUi() }
        </section>
    );
}