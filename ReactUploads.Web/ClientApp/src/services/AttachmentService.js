import { ajax } from 'rxjs/ajax';
import { BehaviorSubject } from 'rxjs';

const attachments = new BehaviorSubject(null);

export default class AttachmentService {
    attachments = attachments.asObservable();

    getAttachments = () => {
        return new Promise((resolve, reject) => {
            ajax.get('/api/attachment/getAttachments')
                .subscribe(
                    ({ response }) => {
                        attachments.next(response);
                        resolve(true);
                    },
                    err => reject(err)
                );
        });
    }

    uploadAttachments = (formData) => {
        return new Promise((resolve) => {
            ajax.post('/api/attachment/uploadAttachments', formData, { 'Accept': 'application/json' })
                .subscribe(
                    () => {
                        this.getAttachments();
                        resolve(true);
                    },
                    err => {
                        console.error(err.error);
                        resolve(false);
                    }
                );
        });
    }

    removeAttachment = (attachment) => {
        return new Promise((resolve) => {
            ajax.post('/api/attachment/removeAttachment', attachment, { 'Content-Type': 'application/json' })
                .subscribe(
                    () => {
                        this.getAttachments();
                        resolve(true);
                    },
                    err => {
                        console.error(err.error);
                        resolve(false);
                    }
                );
        });
    }
}