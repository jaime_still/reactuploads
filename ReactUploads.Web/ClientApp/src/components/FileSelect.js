import React from 'react';

export default (props) => {
    const accept = props.accept || "*/*";
    const label = props.label || 'Browse...';
    const fileInput = React.createRef();

    const inputStyle = {
        display: 'none'
    }

    const onFileSelect = (e) => {
        const files = e.target.files;
        const fileList = [];
        const formData = new FormData();

        for (let i = 0; i < files.length; i++) {
            formData.append(files.item(i).name, files.item(i));
            fileList.push(files.item(i));
        }

        props.onFileSelect([fileList, formData]);
        fileInput.current.value = null;
    }

    const onClick = (e) => {
        e.preventDefault();
        fileInput.current.click();
    }

    return (
        <React.Fragment>
            <input type="file"
                   style={inputStyle}
                   ref={fileInput}
                   accept={accept}
                   onChange={onFileSelect}
                   multiple />
            <button onClick={onClick}>{label}</button>
        </React.Fragment>
    )
}