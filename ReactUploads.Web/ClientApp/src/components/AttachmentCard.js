import React from 'react';
import './styles/attachment-card.css';

export default (props) => {
    const onRemoveAttachment = () => props.onRemoveAttachment(props.attachment);

    return (
        <section className="card-container">
            <a href={props.attachment.url}>{props.attachment.file}</a>
            <button onClick={onRemoveAttachment}>Delete</button>
        </section>
    )
}