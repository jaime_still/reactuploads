import React from 'react';
import FileSelect from './FileSelect';

export default (props) => {
    const onFileSelect = (e) => props.onFileSelect(e);
    const onClearFiles = () => props.onClearFiles();
    const onUpload = () => props.onUpload();

    return (
        <section className="container">
            <FileSelect onFileSelect={onFileSelect} />
            {props.hasFiles &&
                <React.Fragment>
                    <button onClick={onClearFiles} disabled={props.loading}>Clear</button>
                    <button onClick={onUpload} disabled={props.loading}>Upload</button>
                </React.Fragment>
            }
        </section>
    )
}