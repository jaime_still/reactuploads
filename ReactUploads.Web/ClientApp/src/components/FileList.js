import React from 'react';

export default (props) => {
    const renderFiles = () => props.list.map((file) => <li key={file.name}>{file.name}</li>);

    return (
        <React.Fragment>
            {
                props.list && props.list.length > 0 ?
                    <React.Fragment>
                        <h4>Pending Files</h4>
                        <ul>
                            {renderFiles()}
                        </ul>
                    </React.Fragment> :
                    <h4>No Pending Files</h4>
            }
        </React.Fragment>
    )
}