import React from 'react';
import AttachmentCard from './AttachmentCard';

export default (props) => {
    const onRemoveAttachment = (e) => props.onRemoveAttachment(e);

    const renderAttachments = () => (
        <section className="list">
            {props.attachments.map((a) => <AttachmentCard key={a.url} attachment={a} onRemoveAttachment={onRemoveAttachment} />)}
        </section>
    )

    return (
        <React.Fragment>
            {
                props.loading ?
                    <h4>Loading...</h4> :
                    props.attachments && props.attachments.length > 0 ?
                        renderAttachments() :
                        <h4>No attachments available</h4>
            }
        </React.Fragment>
    )
}